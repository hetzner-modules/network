#!/bin/bash

set -e

# check if terraform is formatted
terraform fmt -recursive -check

# check if terraform-docs is up-to-date
terraform-docs markdown table --output-file README.md --output-check .

cd scripts
tflint --init
tflint ..
tflint ../examples