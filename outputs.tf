output "network_id" {
  description = "(int) Unique ID of the network."
  value       = hcloud_network.this.id
}

output "network_name" {
  description = "(string) Name of the network."
  value       = hcloud_network.this.name
}

output "network_ip_range" {
  description = "(string) IPv4 Prefix of the whole Network."
  value       = hcloud_network.this.ip_range
}

output "network_labels" {
  description = "(map) User-defined labels (key-value pairs)"
  value       = hcloud_network.this.labels
}

output "network_delete_protection" {
  description = "(bool) Whether delete protection is enabled."
  value       = hcloud_network.this.delete_protection
}

output "network_subnet_id" {
  description = "(string) ID of the Network subnet."
  value       = values(hcloud_network_subnet.this).*.id
}

output "network_subnet_type" {
  description = "(string) Type of subnet."
  value       = values(hcloud_network_subnet.this).*.type
}

output "network_subnet_ip_range" {
  description = "(string) Range to allocate IPs from."
  value       = values(hcloud_network_subnet.this).*.ip_range
}

output "network_subnet_zone" {
  description = "(string) Name of network zone."
  value       = values(hcloud_network_subnet.this).*.network_zone
}
