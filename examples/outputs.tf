output "network_id" {
  description = "(int) Unique ID of the network."
  value       = module.network.network_id
}

output "network_subnet_id" {
  description = "(int) Unique ID of the network."
  value       = module.network.network_subnet_id
}