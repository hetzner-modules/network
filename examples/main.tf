terraform {
  required_version = ">= 1.2.0"
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/52559932/terraform/state/network"
    lock_address   = "https://gitlab.com/api/v4/projects/52559932/terraform/state/network/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/52559932/terraform/state/network/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
  }

  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.36.1"
    }
  }
}

provider "hcloud" {
  token = var.hcloud_token
}

module "network" {
  source            = "../"
  network_ip_range  = var.network_cidr
  network_name      = var.network_name
  subnet_ip_ranges  = var.subnet_ip_ranges
  delete_protection = var.delete_protection
  network_labels    = var.network_labels
  network_zone      = var.network_zone
  subnet_type       = var.subnet_type
}