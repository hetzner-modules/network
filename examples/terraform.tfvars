# Network name
network_name = "test-network"

# Network to create for private communication
network_cidr = "10.0.0.0/8"

# Subnet to create for private communication. Must be part of the CIDR defined in `network_cidr`.
subnet_ip_ranges = ["10.0.1.0/24"]