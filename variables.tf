############################
######### NETWORK
############################
variable "network_ip_range" {
  type        = string
  description = "(Required, string) IP Range of the whole Network which must span all included subnets and route destinations. Must be one of the private ipv4 ranges of RFC1918."
}

variable "network_name" {
  type        = string
  description = "(Required, string) Name of the Network to create (must be unique per project)."
}

variable "delete_protection" {
  type        = bool
  description = "(Optional, bool) Enable or disable delete protection."
  default     = false
}

variable "network_labels" {
  type        = map(string)
  description = "(Optional, map) User-defined labels (key-value pairs) should be created with."
  default     = {}
}
##############################
####### SUBNET
##############################
variable "subnet_ip_ranges" {
  type        = list(string)
  description = "(Required, list(string)) Range to allocate IPs from. Must be a subnet of the ip_range of the Network and must not overlap with any other subnets or with any destinations in routes."
}

variable "vswitch_subnet_ip_range" {
  type        = string
  description = "(Required, list(string)) Range to allocate IPs from. Must be a subnet of the ip_range of the Network and must not overlap with any other subnets or with any destinations in routes."
  default     = ""
}

variable "vswitch_id" {
  type        = string
  description = "vswitch ID to attach a vswitch to a subnet"
  default     = ""
}

variable "subnet_type" {
  type        = string
  description = "(Required, string) Type of subnet. server, cloud or vswitch"
  default     = "cloud"
}

variable "network_zone" {
  type        = string
  description = "(Required, string) Name of network zone. Eg. eu-central"
  default     = "eu-central"
}
