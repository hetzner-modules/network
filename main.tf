resource "hcloud_network" "this" {
  ip_range          = var.network_ip_range
  name              = var.network_name
  labels            = var.network_labels
  delete_protection = var.delete_protection
}

resource "hcloud_network_subnet" "this" {
  for_each     = toset(var.subnet_ip_ranges)
  network_id   = hcloud_network.this.id
  ip_range     = each.value
  network_zone = var.network_zone
  type         = var.subnet_type
}

resource "hcloud_network_subnet" "vswitch" {
  count        = var.vswitch_id != "" && var.vswitch_subnet_ip_range != "" ? 1 : 0
  network_id   = hcloud_network.this.id
  ip_range     = var.vswitch_subnet_ip_range
  network_zone = var.network_zone
  type         = "vswitch"
}
