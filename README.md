<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.2.0 |
| <a name="requirement_hcloud"></a> [hcloud](#requirement\_hcloud) | >= 1.33.2 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_hcloud"></a> [hcloud](#provider\_hcloud) | >= 1.33.2 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [hcloud_network.this](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/network) | resource |
| [hcloud_network_subnet.this](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/network_subnet) | resource |
| [hcloud_network_subnet.vswitch](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/network_subnet) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_delete_protection"></a> [delete\_protection](#input\_delete\_protection) | (Optional, bool) Enable or disable delete protection. | `bool` | `false` | no |
| <a name="input_network_ip_range"></a> [network\_ip\_range](#input\_network\_ip\_range) | (Required, string) IP Range of the whole Network which must span all included subnets and route destinations. Must be one of the private ipv4 ranges of RFC1918. | `string` | n/a | yes |
| <a name="input_network_labels"></a> [network\_labels](#input\_network\_labels) | (Optional, map) User-defined labels (key-value pairs) should be created with. | `map(string)` | `{}` | no |
| <a name="input_network_name"></a> [network\_name](#input\_network\_name) | (Required, string) Name of the Network to create (must be unique per project). | `string` | n/a | yes |
| <a name="input_network_zone"></a> [network\_zone](#input\_network\_zone) | (Required, string) Name of network zone. Eg. eu-central | `string` | `"eu-central"` | no |
| <a name="input_subnet_ip_ranges"></a> [subnet\_ip\_ranges](#input\_subnet\_ip\_ranges) | (Required, list(string)) Range to allocate IPs from. Must be a subnet of the ip\_range of the Network and must not overlap with any other subnets or with any destinations in routes. | `list(string)` | n/a | yes |
| <a name="input_subnet_type"></a> [subnet\_type](#input\_subnet\_type) | (Required, string) Type of subnet. server, cloud or vswitch | `string` | `"cloud"` | no |
| <a name="input_vswitch_id"></a> [vswitch\_id](#input\_vswitch\_id) | vswitch ID to attach a vswitch to a subnet | `string` | `""` | no |
| <a name="input_vswitch_subnet_ip_range"></a> [vswitch\_subnet\_ip\_range](#input\_vswitch\_subnet\_ip\_range) | (Required, list(string)) Range to allocate IPs from. Must be a subnet of the ip\_range of the Network and must not overlap with any other subnets or with any destinations in routes. | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_network_delete_protection"></a> [network\_delete\_protection](#output\_network\_delete\_protection) | (bool) Whether delete protection is enabled. |
| <a name="output_network_id"></a> [network\_id](#output\_network\_id) | (int) Unique ID of the network. |
| <a name="output_network_ip_range"></a> [network\_ip\_range](#output\_network\_ip\_range) | (string) IPv4 Prefix of the whole Network. |
| <a name="output_network_labels"></a> [network\_labels](#output\_network\_labels) | (map) User-defined labels (key-value pairs) |
| <a name="output_network_name"></a> [network\_name](#output\_network\_name) | (string) Name of the network. |
| <a name="output_network_subnet_id"></a> [network\_subnet\_id](#output\_network\_subnet\_id) | (string) ID of the Network subnet. |
| <a name="output_network_subnet_ip_range"></a> [network\_subnet\_ip\_range](#output\_network\_subnet\_ip\_range) | (string) Range to allocate IPs from. |
| <a name="output_network_subnet_type"></a> [network\_subnet\_type](#output\_network\_subnet\_type) | (string) Type of subnet. |
| <a name="output_network_subnet_zone"></a> [network\_subnet\_zone](#output\_network\_subnet\_zone) | (string) Name of network zone. |
<!-- END_TF_DOCS -->

