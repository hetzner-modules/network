package test

import (
	"fmt"
	"os"
	"testing"

	"github.com/gruntwork-io/terratest/modules/random"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
)

func TestNetwork(t *testing.T) {
	t.Parallel()

	terraformOptions := &terraform.Options{
		TerraformDir: "../examples",
		VarFiles:     []string{"terraform.tfvars"},
		Vars: map[string]interface{}{
			"network_name": fmt.Sprintf("test-network-%s", random.UniqueId()),
		},
		BackendConfig: map[string]interface{}{
			"username": os.Getenv("TF_USERNAME"),
			"password": os.Getenv("ACCESS_TOKEN"),
		},
	}

	defer terraform.Destroy(t, terraformOptions)
	terraform.InitAndApply(t, terraformOptions)
	network_id := terraform.Output(t, terraformOptions, "network_id")
	network_subnet_id := terraform.Output(t, terraformOptions, "network_subnet_id")

	assert.NotEmpty(t, network_id, "Network_id exists")
	assert.NotEmpty(t, network_subnet_id, "Network subnet id does not exist")
}
